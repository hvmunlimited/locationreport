package com.kondrikov.locationreport;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class HistoryActivity extends ListActivity {
	
	DatabaseHelper mDbHelper;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (LocationReportApplication.isLite(this)) {
        	setContentView(R.layout.history_tab_lite);
        	
        	TextView tv = (TextView) findViewById(R.id.upgrade_message);
        	tv.setText(Html.fromHtml(getResources().getString(R.string.lite_history_message)));
        	tv.setMovementMethod(LinkMovementMethod.getInstance());
        } else {
        	setContentView(R.layout.history_tab);
        }
        
        mDbHelper = new DatabaseHelper(this);
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        Cursor cursor = db.query(DatabaseHelper.H.TABLE_NAME, 
        		new String[] {DatabaseHelper.H.ID_FIELD,
        					  DatabaseHelper.H.DISPLAY_NAME,
        					  DatabaseHelper.H.PHONE_NUMBER,
        					  DatabaseHelper.H.RESPONSE,
        					  DatabaseHelper.H.STATUS,
        					  DatabaseHelper.H.TIMESTAMP,
        					  DatabaseHelper.H.LATITUDE,
        					  DatabaseHelper.H.LONGITUDE},
        		null, null, null, null, DatabaseHelper.H.TIMESTAMP + " DESC", null);
        startManagingCursor(cursor);
        
        ListAdapter adapter = new HistoryListAdapter(this, R.layout.history_list_item, cursor,
				new String[] {DatabaseHelper.H.DISPLAY_NAME, DatabaseHelper.H.TIMESTAMP },
				new int[] {R.id.text1, R.id.text2});
        
        setListAdapter(adapter);

        updateLayout();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		mDbHelper.close();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		if (LocationReportApplication.isLite(this)) {
			refreshList();	
		}		
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		Cursor cursor = (Cursor) getListAdapter().getItem(position);

		String status = cursor.getString(cursor.getColumnIndex(DatabaseHelper.H.STATUS));
		
		if (status.equals(DatabaseHelper.H.STATUS_OK) || status.equals(DatabaseHelper.H.STATUS_NO_MAP)) {
			Intent intent = new Intent(this, HistoryItemActivity.class);
			intent.putExtra("response", 
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.H.RESPONSE)));
			intent.putExtra("sender", 
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.H.DISPLAY_NAME)));
			intent.putExtra("time", 
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.H.TIMESTAMP)));
			intent.putExtra("latitude", 
					cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.H.LATITUDE)));
			intent.putExtra("longitude", 
					cursor.getDouble(cursor.getColumnIndex(DatabaseHelper.H.LONGITUDE)));
			intent.putExtra("sender_phone", 
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.H.PHONE_NUMBER)));
			intent.putExtra("status", status);
			startActivity(intent);
		}
	}

	@Override
	public boolean onCreateOptionsMenu (Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.history_menu, menu);
	    
	    return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.refresh_history:
			refreshList();			
	        return true;
	        
		case R.id.clear_history:
			clearHistory();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}

	private void refreshList() {
		CursorAdapter adapter = (CursorAdapter) getListAdapter();
		Cursor cursor = adapter.getCursor(); 

		cursor.requery();
		adapter.notifyDataSetChanged();
		
		updateLayout();
	}
	
	private void clearHistory() {
		mDbHelper.clearHistory();
		refreshList();
	}

	private void updateLayout() {
		if (LocationReportApplication.isLite(this)) {
			TextView tv = (TextView) findViewById(R.id.upgrade_message);
			tv.setVisibility(mDbHelper.historyRowsCount() == 0 ? View.GONE : View.VISIBLE);
		}
	}
}
