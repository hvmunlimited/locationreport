package com.kondrikov.locationreport;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

public class DatabaseHelper extends SQLiteOpenHelper {
	
	private static final String TAG = DatabaseHelper.class.getName();
	
	private static final String DATABASE_NAME = "db";
	private static final int DATABASE_VERSION = 1;

	class H {
		public static final String TABLE_NAME = "history";

		public static final String ID_FIELD 	= "_id";
		public static final String DISPLAY_NAME = "requestFrom";
		public static final String LATITUDE     = "latitude";
		public static final String LONGITUDE    = "longitude";
		public static final String PHONE_NUMBER = "phone";
		public static final String RESPONSE 	= "response";
		public static final String STATUS       = "status";
		public static final String TIMESTAMP	= "timestamp";
		
		public static final String STATUS_REFUSED = "0";		
		public static final String STATUS_OK 	  = "1";
		public static final String STATUS_NO_MAP  = "2";
	}
	
	private static final String HISTORY_TABLE_CREATE = "CREATE TABLE " + H.TABLE_NAME + " (" 
		+ H.ID_FIELD + " INTEGER PRIMARY KEY AUTOINCREMENT, "
		+ H.DISPLAY_NAME + " TEXT, "
		+ H.TIMESTAMP 	 + " INTEGER, "
		+ H.STATUS 	     + " INTEGER, "
		+ H.RESPONSE 	 + " TEXT, "
		+ H.PHONE_NUMBER + " TEXT, "
		+ H.LATITUDE 	 + " REAL, " 
		+ H.LONGITUDE    + " REAL);";

	class U {
		public static final String TABLE_NAME = "users";
		
		public static final String DISPLAY_NAME = "user";
		public static final String KEY = "_id";
	}
	
	private static final String USERS_TABLE_CREATE = "CREATE TABLE " + U.TABLE_NAME + " (" 
		+ U.KEY  + " TEXT PRIMARY KEY, "
		+ U.DISPLAY_NAME + " TEXT);";
	
	ContentResolver mResolver;
	Context mContext;

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);

		mResolver = context.getContentResolver();
		mContext  = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		if (LocationReportApplication.isLogging(mContext)) {
			Log.d(TAG, "onCreate: HISTORY_TABLE_CREATE");
		}
		db.execSQL(HISTORY_TABLE_CREATE);

		if (LocationReportApplication.isLogging(mContext)) {
			Log.d(TAG, "onCreate: USERS_TABLE_CREATE");
		}
		db.execSQL(USERS_TABLE_CREATE);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

	public void insertHistoryItem(String sender, String senderPhone, long timestamp, String status, String reply, double latitude, double longitude) {
		
		SQLiteDatabase db = getWritableDatabase();
		
		try {
			if (LocationReportApplication.isLite(mContext)) {
				db.delete(H.TABLE_NAME, null, null);
			}

			ContentValues values = new ContentValues();

			values.put(DatabaseHelper.H.DISPLAY_NAME, sender);
			values.put(DatabaseHelper.H.LATITUDE, latitude);
			values.put(DatabaseHelper.H.LONGITUDE, longitude);
			values.put(DatabaseHelper.H.PHONE_NUMBER, senderPhone);
			values.put(DatabaseHelper.H.RESPONSE, reply);
			values.put(DatabaseHelper.H.STATUS, status);
			values.put(DatabaseHelper.H.TIMESTAMP, Long.toString(timestamp));

			db.insert(DatabaseHelper.H.TABLE_NAME, null, values);
		}
		catch (SQLiteException e) {
		}
	}

	public void insertUser(String key, String name) {
		
		SQLiteDatabase db = getWritableDatabase();
		
		try {
			boolean allowed = true;
			if (LocationReportApplication.isLite(mContext)) {
				if (DatabaseUtils.queryNumEntries(db, DatabaseHelper.U.TABLE_NAME) == 1) {
					allowed = false;
				}
			}

			if (allowed) {
				ContentValues values = new ContentValues();
				values.put(DatabaseHelper.U.KEY, key);
				values.put(DatabaseHelper.U.DISPLAY_NAME, name);

				db.insert(DatabaseHelper.U.TABLE_NAME, null, values);
			} else {
				Toast.makeText(mContext, R.string.lite_user_limit, Toast.LENGTH_SHORT).show();
			}
		}
		catch (SQLiteException e) {
		}
	}
	

	public void removeUser(String key) {
		SQLiteDatabase db = getWritableDatabase();
		
		try {
			db.delete(U.TABLE_NAME, U.KEY + " = ?", new String[] { key });
		}
		catch (SQLiteException e) {
		}		
	}


	public boolean hasUser(String testKey) {
		SQLiteDatabase db = getReadableDatabase();
		
		boolean result = false;
		
		Cursor cursor = null;
		
		try {
			Uri testLookupUri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, testKey);
			Uri testUri = ContactsContract.Contacts.lookupContact(mResolver, testLookupUri);
			if (testUri == null) {
				throw new RuntimeException("Invalid testKey");
			}
			
			cursor = mResolver.query(testUri, new String[] {ContactsContract.Contacts._ID}, null, null, null);
			if (cursor == null || !cursor.moveToFirst()) {
				throw new RuntimeException("Invalid query");
			}
			
			long testId = cursor.getLong(cursor.getColumnIndex(ContactsContract.Contacts._ID));
			
			Cursor itemsCursor = db.query(U.TABLE_NAME, new String[] { U.KEY }, null, null, null, null, null);
			try {
				if (itemsCursor != null && itemsCursor.moveToFirst()) {
					do {
	        			Uri lookupUri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI,
	        					itemsCursor.getString(itemsCursor.getColumnIndex(U.KEY)));
	        			
	        			Uri contactUri = ContactsContract.Contacts.lookupContact(mResolver, lookupUri);
	        			if (contactUri != null) {
	    					Cursor itemCursor = mResolver.query(contactUri, new String[] {ContactsContract.Contacts._ID}, null, null, null);
	        				try {
		    					if (itemCursor != null && itemCursor.moveToFirst()) {
			    					long id = itemCursor.getLong(itemCursor.getColumnIndex(ContactsContract.Contacts._ID));
			    					if (id == testId) {
			    						result = true;
			    						break;
			    					}
		    					}
	        				}
	        				finally {
	        					itemCursor.close();
	        				}
	        			}
					} while (itemsCursor.moveToNext());
				}
			}
			
			finally {
				itemsCursor.close();
			}
		}
		
		catch (RuntimeException e) {
			if (LocationReportApplication.isLogging(mContext)) {
				Log.e(TAG, e.getMessage());
			}
		}
		
		finally {
			cursor.close();
		}

		return result;
	}

	public void clearHistory() {
		SQLiteDatabase db = getWritableDatabase();
		
		try {
			db.delete(H.TABLE_NAME, null, null);
		}
		catch (SQLiteException e) {
		}
	}

	public void clearUsers() {
		SQLiteDatabase db = getWritableDatabase();
		
		try {
			db.delete(U.TABLE_NAME, null, null);
		}
		catch (SQLiteException e) {
		}
	}

	public long historyRowsCount() {
		SQLiteDatabase db = getReadableDatabase();

		try {
			return DatabaseUtils.queryNumEntries(db, DatabaseHelper.H.TABLE_NAME);
		}
		catch (SQLiteException e) {
		}

		return 0;
	}

	public long userRowsCount() {
		SQLiteDatabase db = getReadableDatabase();

		try {
			return DatabaseUtils.queryNumEntries(db, DatabaseHelper.U.TABLE_NAME);
		}
		catch (SQLiteException e) {
		}

		return 0;
	}

}
