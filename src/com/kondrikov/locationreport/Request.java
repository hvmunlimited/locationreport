package com.kondrikov.locationreport;

public class Request {

	private long   mTimestamp;
	private String mContents;
	private String mSender;
	private String mName;
	
	private int mStartId;
	
	public Request(String name, String sender, String contents, long timestamp) {
		mTimestamp = timestamp;
		mContents = contents;
		mSender = sender;
		mName = name;
	}
	
	public boolean isEmpty() {
		return mSender == null;
	}
	
	public Request() {
	}

	public String getSender() {
		return mSender;
	}

	public String getName() {
		return mName;
	}

	public long getTimestamp() {
		return mTimestamp;
	}

	public String getContents() {
		return mContents;
	}

	public int getStartId() {
		return mStartId;
	}

	public void setStartId(int id) {
		mStartId = id; 
	}

	public void appendContents(String str) {
		mContents += str;
	}
}

