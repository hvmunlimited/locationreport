package com.kondrikov.locationreport;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class SettingsActivity extends PreferenceActivity {
	
	private static final String TAG = SettingsActivity.class.getName();
	
	private int mDebugId;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        addPreferencesFromResource(R.xml.prefs);
    	
        mDebugId = getResources().getIdentifier("id/debug", null, this.getPackageName());
	}
	
	@Override
	public boolean onCreateOptionsMenu (Menu menu) {
	    MenuInflater inflater = getMenuInflater();

	    inflater.inflate(R.menu.main_menu, menu);

	    if (LocationReportApplication.isDebugging()) {
	    	menu.add(Menu.NONE, mDebugId, Menu.NONE, "DEBUG");
	    }
	    
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.rate:
			onRateIt();
	        return true;
	        
	    case R.id.translate:
	    	onTranslate();
	        return true;
	        
		case R.id.feedback:
			onFeedback();
			return true;
		
		case R.id.more:
			onMore();
			return true;
		}
	
		if (item.getItemId() == mDebugId) {
			onDebugMenu();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}

	private void onDebugMenu() {
		if (LocationReportApplication.isDebugging()) {
			Intent service = new Intent(this, LocationService.class);
			service.setAction(LocationService.START_SERVICE);
	
			service.putExtra("sender", "1234");
			service.putExtra("timestamp", System.currentTimeMillis());
	
			startService(service);
		}
	}

	private void onMore() {
		startActivity(new Intent(this, AdvancedSettingsActivity.class));
	}

	private void onFeedback() {
		try {
		    PackageInfo manager = getPackageManager().getPackageInfo(getPackageName(), 0);
			String subject = getPackageName() + ", " + manager.versionName;

			Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
			emailIntent.setType("plain/text");
			emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] {"support@kondrikov.com"});
			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
			
			startActivity(emailIntent);
		} 
		
		catch (NameNotFoundException e) {
			if (LocationReportApplication.isLogging(this)) {
				Log.e(TAG, "onFeedback: Name not found!");
			}
		}
		
		catch (ActivityNotFoundException e) {
			if (LocationReportApplication.isLogging(this)) {
				Log.e(TAG, "onFeedback: Could not launch email activity!");
			}
		}		
	}

	private void onTranslate() {
		new AlertDialog.Builder(this)
			.setTitle(R.string.translate_dialog_title)
			.setMessage(R.string.translate_dialog_message)
			.setPositiveButton(R.string.translate_dialog_positive, new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
				    PackageInfo manager;
					try {
						manager = getPackageManager().getPackageInfo(getPackageName(), 0);
						
						String subject = getResources().getString(R.string.translate_subject) +
								getPackageName() + ", " + manager.versionName;
		
						Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
						emailIntent.setType("plain/text");
						
						emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
								new String[] {"support@kondrikov.com"});
						emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
								subject);
						emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,
								getResources().getString(R.string.translate_message));
						
						startActivity(emailIntent);
						
					} catch (NameNotFoundException e) {
						if (LocationReportApplication.isLogging(SettingsActivity.this)) {
							Log.e(TAG, "TranslateDialog: NameNotFoundException");
						}
					} catch (ActivityNotFoundException e) {
						if (LocationReportApplication.isLogging(SettingsActivity.this)) {
							Log.e(TAG, "TranslateDialog: ActivityNotFoundException");
						}
					}		
				}
			})
			.setNegativeButton(R.string.translate_dialog_negative, null)
			.show();
	}
	
	private void onRateIt() {
		Uri uri = Uri.parse("market://details?id=" + getPackageName());
		Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
		
		try {
		    startActivity(goToMarket);
		} 
		
		catch (ActivityNotFoundException e) {
			if (LocationReportApplication.isLogging(this)) {
				Log.d(TAG, "Could not launch market");
			}
		}
	}
}
