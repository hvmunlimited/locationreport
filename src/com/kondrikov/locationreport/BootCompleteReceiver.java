package com.kondrikov.locationreport;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BootCompleteReceiver extends BroadcastReceiver {

	private static final String TAG = BootCompleteReceiver.class.getName();
	
		@Override
		public void onReceive(Context context, Intent intent) {
			if (LocationReportApplication.isLogging(context)) {
				Log.d(TAG, "onReceive"); 
			}
			
			Intent serviceIntent = new Intent(context, UpdateDbService.class);
			context.startService(serviceIntent);
		}
}
