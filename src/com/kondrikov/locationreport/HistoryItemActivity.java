package com.kondrikov.locationreport;

import java.io.InputStream;
import java.sql.Date;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.provider.ContactsContract.PhoneLookup;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.format.DateFormat;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.QuickContactBadge;
import android.widget.TextView;

import com.viewpagerindicator.TitlePageIndicator;
import com.viewpagerindicator.TitleProvider;

public class HistoryItemActivity extends Activity {

	private String mResponse;
	private String mStatus;
	private String mTime;
	
	private double mLongitude;
	private double mLatitude;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.history_activity_item);

		Intent intent = getIntent();
		
		mStatus = intent.getStringExtra("status");
		mResponse = intent.getStringExtra("response");

		Date date = new Date(intent.getLongExtra("time", 0));
		mTime = DateFormat.getDateFormat(this).format(date) + ", "
			  + DateFormat.getTimeFormat(this).format(date);
		
		mLatitude = intent.getDoubleExtra("latitude", 0);
		mLongitude = intent.getDoubleExtra("longitude", 0);

		TextView tvSender = (TextView) findViewById(R.id.title);
		String displayName = intent.getStringExtra("sender");
		tvSender.setText(displayName);
		
		QuickContactBadge badge = (QuickContactBadge) findViewById(R.id.contact_badge);
		String senderPhone = intent.getStringExtra("sender_phone");
		
		badge.assignContactFromPhone(senderPhone, true);
		setContactDrawable(senderPhone, badge);

		ViewPager pager = (ViewPager) findViewById(R.id.pager);
		ItemPagerAdapter adapter = new ItemPagerAdapter();
		pager.setAdapter(adapter);

		TitlePageIndicator indicator = (TitlePageIndicator) findViewById(R.id.indicator);
		indicator.setViewPager(pager);
	}

	private void setContactDrawable(String senderPhone, QuickContactBadge badge) {
		try {
			ContentResolver resolver = getContentResolver();
			
			Uri lookupUri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(senderPhone));
			Uri uri = ContactsContract.Contacts.lookupContact(resolver, lookupUri);
			
			if (uri != null) {
				InputStream is = ContactsContract.Contacts.openContactPhotoInputStream(resolver, uri);
				if (is != null) {
					badge.setImageDrawable(Drawable.createFromStream(is, "picture"));
				}
			}
		}
		
		catch (Exception e) {
			badge.setImageDrawable(getResources().getDrawable(R.drawable.ic_contact_picture));
			e.printStackTrace();
		}
	}

	private class ItemPagerAdapter extends PagerAdapter implements TitleProvider {

		public String getTitle(int position) {
			return position == 0 ? getResources().getText(R.string.title_message).toString() 
								 : getResources().getText(R.string.title_map).toString();
		}

		@Override
		public int getCount() {
			return 2;
		}

		@Override
		public Object instantiateItem(View collection, int position) {
			LayoutInflater inflater = (LayoutInflater) HistoryItemActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view;
			
			if (position == 0) {
				view = inflater.inflate(R.layout.pager_message, null);

				TextView tvSender = (TextView) view.findViewById(R.id.message_time);
				tvSender.setText(mTime);

				TextView tvMessage = (TextView) view.findViewById(R.id.message_content);
				tvMessage.setAutoLinkMask(Linkify.WEB_URLS);
				tvMessage.setText(mResponse);
			} else {
				if (mStatus.equals(DatabaseHelper.H.STATUS_OK)) {
					view = inflater.inflate(R.layout.pager_map, null);
					
					MapPageLayout mapLayout = (MapPageLayout) view.findViewById(R.id.map_page_layout);
					mapLayout.initLocation(mLatitude, mLongitude);
				}
				else {
					view = inflater.inflate(R.layout.map_placeholder, null);
				}
			}

			((ViewPager) collection).addView(view, 0);

			return view;
		}

		@Override
		public void destroyItem(View collection, int position, Object view) {
			((ViewPager) collection).removeView((View) view);
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == ((View) object);
		}

		@Override
		public void finishUpdate(View arg0) { }

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) { }

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View arg0) { }
	}
}
