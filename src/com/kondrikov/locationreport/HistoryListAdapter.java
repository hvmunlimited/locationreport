package com.kondrikov.locationreport;

import java.util.Date;

import android.content.Context;
import android.database.Cursor;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class HistoryListAdapter extends SimpleCursorAdapter {
	
	private int layout;

	public HistoryListAdapter(Context context, int layout, Cursor c,
			String[] from, int[] to) {
		super(context, layout, c, from, to);
		this.layout = layout;
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		
		View view = inflater.inflate(layout, null);
		
		HistoryItemWrapper tag = new HistoryItemWrapper(view);
		view.setTag(tag);		
				  
		return view;
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		HistoryItemWrapper wrapper = (HistoryItemWrapper) view.getTag();
		
		String requestFrom = cursor.getString(cursor.getColumnIndex(DatabaseHelper.H.DISPLAY_NAME));
		wrapper.getText1().setText(requestFrom);
		
		long time = cursor.getLong(cursor.getColumnIndex(DatabaseHelper.H.TIMESTAMP));
		Date date = new Date(time);
		wrapper.getText2().setText(DateFormat.getDateFormat(context).format(date) + ", " + 
								   DateFormat.getTimeFormat(context).format(date));
		
		String status = cursor.getString(cursor.getColumnIndex(DatabaseHelper.H.STATUS));
		boolean accepted = (status.equals(DatabaseHelper.H.STATUS_OK) || status.equals(DatabaseHelper.H.STATUS_NO_MAP));
		
		wrapper.getMark().setImageDrawable(context.getResources().getDrawable(
				accepted ? R.drawable.ic_menu_more : R.drawable.ic_menu_stop));
	}
	
	private class HistoryItemWrapper {
		
		public HistoryItemWrapper(View view) {
			item = view;
		}
		
		public TextView getText1() {
			if (text1 == null) {
				text1 = (TextView) item.findViewById(R.id.text1);
			}
			
			return text1;
		}

		public TextView getText2() {
			if (text2 == null) {
				text2 = (TextView) item.findViewById(R.id.text2);
			}
			
			return text2;
		}
		
		public ImageView getMark() {
			if (mark == null) {
				mark = (ImageView) item.findViewById(R.id.mark);
			}
			
			return mark;
		}
	
		private TextView text1;
		private TextView text2;
		private ImageView mark;
		
		private View item;
	}

}
