package com.kondrikov.locationreport;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/** Common application settings */
public class LocationReportApplication {
	private static SharedPreferences mPrefs = null;
	
	/** Is logging enabled */
	public static boolean isLogging(Context c) {
		if (mPrefs == null) {
			mPrefs = PreferenceManager.getDefaultSharedPreferences(c); 
		}
		
		if (mPrefs.getBoolean("debug_log_mode", false)) {
			return true;
		}
		
		return false;
	}

	/** Is debugging mode enabled */
	public static boolean isDebugging() {
		return false;
	}

	/** Is free version */
	public static boolean isLite(Context c) {
		return c.getPackageName().contains(".lite");
	}
}
