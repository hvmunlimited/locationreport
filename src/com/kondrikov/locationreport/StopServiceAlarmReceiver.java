package com.kondrikov.locationreport;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/** Broadcast receiver to stop LocationService */
public class StopServiceAlarmReceiver extends BroadcastReceiver {
	private static final String TAG = StopServiceAlarmReceiver.class.getName();

	/** Broadcast received */
	@Override
	public void onReceive(Context context, Intent intent) {
		if (LocationReportApplication.isLogging(context)) {
			Log.d(TAG, "Timeout expired");
		}
		
		Intent service = new Intent(context, LocationService.class);
		service.setAction(LocationService.STOP_SERVICE);
		context.startService(service);
	}

}
