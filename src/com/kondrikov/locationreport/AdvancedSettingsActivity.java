package com.kondrikov.locationreport;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

/** Activity for troubleshooting preferences */
public class AdvancedSettingsActivity extends PreferenceActivity {
	
	private final static String TAG = AdvancedSettingsActivity.class.getName();

	/** ProgressDialog to indicate log collection progress */
	private ProgressDialog mProgressDialog;

	/** Called when activity is starting */
	@Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        
        addPreferencesFromResource(R.xml.prefs_advanced);
	}
	
	/** Called when activity wants to create its options menu */
	@Override
	public boolean onCreateOptionsMenu (Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.advanced_menu, menu);

	    return true;
	}

	/** Called when item from options menu selected*/
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.item_send_log:
			onSendLog();
	        return true;
		}
	
		return super.onOptionsItemSelected(item);
	}

	/** Send LogCat to support email address */
	private void onSendLog() {
		new CollectLogTask().execute();
	}
	
	/** AsyncTask to collect LogCat log */
	private class CollectLogTask extends AsyncTask<Void, Void, StringBuilder> {
		
		/** Called to setup task */
		@Override
		protected void onPreExecute() {
	        showProgressDialog();
		}
		
		/** Perform background log collection */
		@Override
		protected StringBuilder doInBackground(Void... arg0) {
			final StringBuilder log = new StringBuilder();

			// Device properties
			final String separator = System.getProperty("line.separator");
			log.append("DEVICE MODEL: ");
			log.append(Build.MODEL + separator);
			log.append("VERSION: ");
			log.append(Build.VERSION.RELEASE + separator);
			log.append("BUILD: ");
			log.append(Build.DISPLAY + separator);
			
			// LogCat output
            try {
                Process process = Runtime.getRuntime().exec(
                        new String[] { "logcat", "-d", "-v", "time", logcatFilter() });
                
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                		process.getInputStream()));
                
                String line;
                while ((line = bufferedReader.readLine()) != null) { 
                    log.append(line);
                    log.append(separator); 
                }
                
                log.append(checksum(log));
            } 
            catch (IOException e){
            	if (LocationReportApplication.isLogging(AdvancedSettingsActivity.this)) {
            		Log.e(TAG, "CollectLogTask.doInBackground failed!", e);
            	}
            } 

			return log;
		}

		/** Create md5-hash of a log */
		private String checksum(StringBuilder log) {
		    try {
		        MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
		        digest.update(log.toString().getBytes());

		        return new BigInteger(1, digest.digest()).toString(16);
		        
		    } catch (NoSuchAlgorithmException e) {
		    	if (LocationReportApplication.isLogging(AdvancedSettingsActivity.this)) {
		    		Log.w(TAG, "Can't create hash. Algorithm is not available");
		    	}
		    }

		    return "";
		}

		/** Background log collecting is finished */
		@Override
		protected void onPostExecute(StringBuilder result) {
			dismissProgressDialog();
			
			send(result);
		}

	}

	/** Show progress dialog */
	protected void showProgressDialog() {
		mProgressDialog = new ProgressDialog(AdvancedSettingsActivity.this);
		mProgressDialog.setMessage(getString(R.string.message_collect_log));
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}

	/** Dismiss progress dialog */
	protected void dismissProgressDialog() {
		mProgressDialog.dismiss();
		mProgressDialog = null;
	}
	
	/** Send LogCat output by email */
	public void send(StringBuilder result) {
		try {
		    PackageInfo manager = getPackageManager().getPackageInfo(getPackageName(), 0);
			String subject = getPackageName() + ", " + manager.versionName;

			Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
			emailIntent.setType("plain/text");
			emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] {"support@kondrikov.com"});
			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
			emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, result.toString());

			startActivity(emailIntent);
		} 
		
		catch (NameNotFoundException e) {
			if (LocationReportApplication.isLogging(this)) {
				Log.e(TAG, "AdvancedSettingsActivity.send: Name not found!", e);
			}
		}
		
		catch (ActivityNotFoundException e) {
			if (LocationReportApplication.isLogging(this)) {
				Log.e(TAG, "AdvancedSettingsActivity.send: Could not launch email activity!");
			}
		}		
	}

	/** Return filter for LogCat output */
	private String logcatFilter() {
		final StringBuilder result = new StringBuilder();

		result.append(LocationReportActivity.class.getName());
		result.append(":D ");
		result.append(LocationService.class.getName());
		result.append(":D ");
		result.append(Locator.class.getName());
		result.append(":D ");
		result.append(SmsReceiver.class.getName());
		result.append(":D ");
		result.append(StopServiceAlarmReceiver.class.getName());
		result.append(":D ");
		
		result.append("*:S");
				
		return result.toString();
	}

}
