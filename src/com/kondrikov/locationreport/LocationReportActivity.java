package com.kondrikov.locationreport;

import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.widget.TabHost;
import android.widget.Toast;

public class LocationReportActivity extends TabActivity {
	
	private static final String TAG = LocationReportActivity.class.getName();
	private static final int SETTINGS_ACTIVITY = 1;
	
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.main);
        
        Intent service = new Intent(this, UpdateDbService.class);
        startService(service);
        
        if (LocationReportApplication.isLogging(this)) {
        	Log.d(TAG, "onCreate");
        }
        
        Resources res = getResources();
        TabHost tabHost = getTabHost();
        Intent intent;
        
        intent = new Intent(this, SettingsActivity.class);
        tabHost.addTab(tabHost.newTabSpec("settings").setIndicator(getResources().getText(R.string.settings_title), res.getDrawable(R.drawable.ic_tab_settings)).setContent(intent));

        intent = new Intent(this, HistoryActivity.class);
        tabHost.addTab(tabHost.newTabSpec("history").setIndicator(getResources().getText(R.string.history_title), res.getDrawable(R.drawable.ic_tab_history)).setContent(intent));

        intent = new Intent(this, UsersActivity.class);
        tabHost.addTab(tabHost.newTabSpec("users").setIndicator(getResources().getText(R.string.users_title), res.getDrawable(R.drawable.ic_tab_users)).setContent(intent));
        
        tabHost.setCurrentTab(0);
        
        checkSettings();
        checkFirstRun();
    }

	private void checkFirstRun() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		if (prefs.getBoolean("hidden_firstRun", true)) {
			
			// First run
			// ...
			
			Editor editor = prefs.edit();
			editor.putBoolean("hidden_firstRun", false);
			editor.commit();
			
		}
	}

	private void checkSettings() {
		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		
		try {
			boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			boolean gps 	= locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
			if (!network || !gps) {
				new AlertDialog.Builder(this)
					.setTitle(R.string.dialog_title)
					.setMessage(R.string.dialog_message)
					.setPositiveButton(R.string.dialog_positive, new DialogInterface.OnClickListener() {
						
						public void onClick(DialogInterface dialog, int which) {
							try {
								startSettingsActivity();
							} catch (ActivityNotFoundException e) {
								if (LocationReportApplication.isLogging(LocationReportActivity.this)) {
									Log.e(TAG, "Can't start settings activity!");
								}
							}
						}

						/** Start activity with location settings */
						private void startSettingsActivity() {
							try {
								Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
								startActivityForResult(intent, SETTINGS_ACTIVITY);
							} catch (ActivityNotFoundException e) {
								Intent intent = new Intent(Settings.ACTION_SECURITY_SETTINGS);
								startActivityForResult(intent, SETTINGS_ACTIVITY);
							}
						}
					})
					.setNegativeButton(R.string.dialog_negative, new DialogInterface.OnClickListener() {
						
						public void onClick(DialogInterface dialog, int which) {
							showLocationWarning();
						}
					})
					.setCancelable(false)
					.show();
			}
			
			if (LocationReportApplication.isLogging(this)) {
				Log.d(TAG, "gps: " + gps + "; network: " + network);
			}
			
		} catch (Exception e) {
			if (LocationReportApplication.isLogging(this)) {
				Log.e(TAG, "Exception: checkSettings");
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == SETTINGS_ACTIVITY && resultCode == 0) {
			LocationManager locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
			
			boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			boolean gps 	= locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
			if (!network || !gps) {
				showLocationWarning();
			}
			
			if (LocationReportApplication.isLogging(this)) {
				Log.d(TAG, "gps: " + gps + "; network: " + network);
			}
			
		}
	}

	protected void showLocationWarning() {
		Toast.makeText(this, R.string.settings_warning, Toast.LENGTH_LONG).show();
	}
}