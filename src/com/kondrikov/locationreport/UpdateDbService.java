package com.kondrikov.locationreport;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.provider.ContactsContract.PhoneLookup;
import android.util.Log;

public class UpdateDbService extends Service {
	
	private final static String TAG = UpdateDbService.class.getName();
	private DatabaseHelper mDbHelper;
	
	ContactsObserver mObserver = new ContactsObserver();
	
	@Override
	public void onCreate() {
		mDbHelper = new DatabaseHelper(this);
		
		if (LocationReportApplication.isLogging(this)) {
			Log.d(TAG, "onCreate: ContentResolver.registerContentObserver");
		}
		getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true, mObserver);
	}
	
	@Override
	public void onDestroy() {
		if (LocationReportApplication.isLogging(this)) {
			Log.d(TAG, "onDestroy: ContentResolver.unregisterContentObserver");
		}
		getContentResolver().unregisterContentObserver(mObserver);
		mDbHelper.close();
	}
	
	@Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }	

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	
	private class ContactsObserver extends ContentObserver {
		
		public ContactsObserver() {
			super(new Handler());
		}

		@Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);

            updateHistoryTable();
        	updateUsersTable();

        }

		private void updateHistoryTable() {
			SQLiteDatabase db = mDbHelper.getWritableDatabase();
			
        	Cursor itemsCursor = db.query(DatabaseHelper.H.TABLE_NAME, new String[] {DatabaseHelper.H.ID_FIELD, DatabaseHelper.H.DISPLAY_NAME, DatabaseHelper.H.PHONE_NUMBER}, null, null, null, null, null);
        	try {
            	if (itemsCursor != null && itemsCursor.moveToFirst()) {
            		do {
            			String displayName = itemsCursor.getString(itemsCursor.getColumnIndex(DatabaseHelper.H.DISPLAY_NAME));
            			String phoneNumber = itemsCursor.getString(itemsCursor.getColumnIndex(DatabaseHelper.H.PHONE_NUMBER));

        				Cursor contactCursor = getContentResolver().query(Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber)),
        						new String[] {PhoneLookup.DISPLAY_NAME}, null, null, null);
            			try {
                			long currentId = itemsCursor.getLong(itemsCursor.getColumnIndex(DatabaseHelper.H.ID_FIELD));
                			
	        				if (contactCursor != null && contactCursor.moveToFirst()) {
	        					String contactName = contactCursor.getString(contactCursor.getColumnIndex(PhoneLookup.DISPLAY_NAME));
	        					if (!displayName.equals(contactName)) {
	        						ContentValues values = new ContentValues();
	        						values.put(DatabaseHelper.H.DISPLAY_NAME, contactName);

	        						db.update(DatabaseHelper.H.TABLE_NAME, values, DatabaseHelper.H.ID_FIELD + " = ?", new String[] { Long.toString(currentId) });
	        					}
	        				}
	        				else {
	        					ContentValues values = new ContentValues();
	    						values.put(DatabaseHelper.H.DISPLAY_NAME, phoneNumber);
	    						
	    						db.update(DatabaseHelper.H.TABLE_NAME, values, DatabaseHelper.H.ID_FIELD + " = ?", new String[] { Long.toString(currentId) });
	        				}
            			}
            			finally {
            				contactCursor.close();
            			}
	
            		} while (itemsCursor.moveToNext());
            	}
        	}
            catch (Exception e) {
            }
            
            finally {
            	itemsCursor.close();
            }
		}
		
		private void updateUsersTable() {
			SQLiteDatabase db = mDbHelper.getWritableDatabase();
        	
			Cursor itemsCursor = db.query(DatabaseHelper.U.TABLE_NAME, new String[] {DatabaseHelper.U.KEY, DatabaseHelper.U.DISPLAY_NAME}, null, null, null, null, null);
        	try {
            	if (itemsCursor != null && itemsCursor.moveToFirst()) {
            		do {
            			String currName = itemsCursor.getString(itemsCursor.getColumnIndex(DatabaseHelper.U.DISPLAY_NAME));
            			String currKey = itemsCursor.getString(itemsCursor.getColumnIndex(DatabaseHelper.U.KEY));

            			Uri contactUri = ContactsContract.Contacts.lookupContact(getContentResolver(),
            					Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, currKey));

            			if (contactUri != null) {
	            			Cursor itemCursor = getContentResolver().query(contactUri, new String[] {ContactsContract.Contacts.DISPLAY_NAME}, null, null, null);
	            			try {
		        				if (itemCursor != null && itemCursor.moveToFirst()) {
		        					String actualName = itemCursor.getString(itemCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
		        					if (!currName.equals(actualName)) {
		        						ContentValues values = new ContentValues();
		        						values.put(DatabaseHelper.U.DISPLAY_NAME, actualName);
		        						
		        						db.update(DatabaseHelper.U.TABLE_NAME, values, DatabaseHelper.U.KEY + " = ?", new String[] { currKey });
		        					}
		        				}
	            			}
	            			finally {
	            				itemCursor.close();
	            			}
            			}
            			else {
    						ContentValues values = new ContentValues();
    						values.put(DatabaseHelper.U.DISPLAY_NAME, "");
    						
    						db.update(DatabaseHelper.U.TABLE_NAME, values, DatabaseHelper.U.KEY + " = ?", new String[] { currKey });
            			}
	
            		} while (itemsCursor.moveToNext());
            	}
        	}
            catch (RuntimeException e) {
            }
            
            finally {
            	itemsCursor.close();
            }
            
            db.delete(DatabaseHelper.U.TABLE_NAME, DatabaseHelper.U.DISPLAY_NAME + " = ?", new String[] {""});
		}
	}
}
