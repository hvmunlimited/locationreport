package com.kondrikov.locationreport;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class MapPageLayout extends FrameLayout {

	private final String urlPattern = "http://maps.googleapis.com/maps/api/staticmap" +
			"?scale=2" +
			"&sensor=true" +
			"&format=jpg" +
			"&maptype=roadmap" +
			"&zoom=16" +
			"&key=ABQIAAAAuXk1M98pft8AEP-oU8NpNhRmWr_EnfEN0pQ5RH1HePFxu8qT3BS22SQ1SwLN1z9fVXuyYcs6_-Zlcg";
	
	private double mLongitude = 0;
	private double mLatitude = 0;
	
	private ImageView mMapImageView;

	public MapPageLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);

		if (changed) {
			mMapImageView = (ImageView) findViewById(R.id.mapImage);

			findViewById(R.id.download_progress).setVisibility(View.VISIBLE);
			findViewById(R.id.download_status).setVisibility(View.GONE);
			mMapImageView.setVisibility(View.GONE);

			int w = r - l;	if (w > 640) w = 640;
			int h = b - t;	if (h > 640) h = 640;

			String url = urlPattern + String.format(Locale.ENGLISH, 
					"&size=%dx%d&markers=%f,%f", (int) (w / 2.0), (int) (h / 2.0), mLatitude, mLongitude);		
			
			fetchAsync(url, w, h);
		}
	}

	private void fetchAsync(final String urlString, int w, int h) {

		final Handler handler = new Handler() {
			@Override
			public void handleMessage(Message message) {
				
				findViewById(R.id.download_progress).setVisibility(View.GONE);
				if (message.obj != null) {
					mMapImageView.setImageDrawable((Drawable) message.obj);
					mMapImageView.setVisibility(View.VISIBLE);
				} else {
					findViewById(R.id.download_status).setVisibility(View.VISIBLE);
				}
			}
		};

		Thread thread = new Thread() {
			@Override
			public void run() {
				Drawable drawable = fetchDrawable(urlString);
				Message message = handler.obtainMessage(1, drawable);
				handler.sendMessage(message);
			}
		};

		thread.start();
	}

	private Drawable fetchDrawable(String urlString) {
		try {
			InputStream istream = fetch(urlString);
			Drawable drawable = Drawable.createFromStream(istream, "map");
			
			return drawable;

		}
		
		catch (MalformedURLException e) {
			return null;
		} 
		
		catch (IOException e) {
			return null;
		}
	}

	private InputStream fetch(String urlString) throws MalformedURLException, IOException {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet request = new HttpGet(urlString);
		
		HttpResponse response = httpClient.execute(request);
		return response.getEntity().getContent();
	}

	public void initLocation(double latitude, double longitude) {
		mLongitude = longitude;
		mLatitude = latitude;
	}
}
