package com.kondrikov.locationreport;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ListAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class UsersActivity extends ListActivity {
	
	private static final String TAG = UsersActivity.class.getName();

	private static int CONTACT_PICKER_RESULT = 1;
	private DatabaseHelper mDbHelper;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (LocationReportApplication.isLite(this)) {
        	setContentView(R.layout.users_tab_lite);
        	
        	TextView tv = (TextView) findViewById(R.id.upgrade_message);
        	tv.setText(Html.fromHtml(getResources().getString(R.string.lite_users_message)));
        	tv.setMovementMethod(LinkMovementMethod.getInstance());
        } else {
        	setContentView(R.layout.users_tab);
        }
        
        mDbHelper = new DatabaseHelper(this);
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        Cursor cursor = db.query(DatabaseHelper.U.TABLE_NAME, 
        		new String[] {DatabaseHelper.U.KEY, DatabaseHelper.U.DISPLAY_NAME}, null, null, null, null,
        		DatabaseHelper.U.DISPLAY_NAME, null);
        startManagingCursor(cursor);
        
        ListAdapter adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, cursor,
        		new String[] {DatabaseHelper.U.DISPLAY_NAME},
        		new int[] {android.R.id.text1});
        
        setListAdapter(adapter);
        updateLayout();
        
        registerForContextMenu(getListView());
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		mDbHelper.close();
	}
	
	@Override
	public boolean onCreateOptionsMenu (Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.users_menu, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add_user:
			addUser();
	        return true;
	        
		case R.id.clear_users:
			clearUsers();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.user_menu, menu);
		
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
		menu.setHeaderTitle(((TextView) info.targetView).getText());
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		  switch (item.getItemId()) {
			  case R.id.remove_from_list:
			    removeUser(info.position);
			    return true;
			  default:
			    return super.onContextItemSelected(item);
		  }
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
	    if (resultCode == RESULT_OK && requestCode == CONTACT_PICKER_RESULT) {

	    	Uri userUri = data.getData();
	    	Cursor cursor = getContentResolver().query(userUri, new String[] {ContactsContract.Contacts.LOOKUP_KEY, ContactsContract.Contacts.DISPLAY_NAME}, null, null, null);

	    	try {
		    	if (cursor.moveToFirst())
		    	{
		    		String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
		    		String key = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));

			    	mDbHelper.insertUser(key, name);
			    	refreshList();

			    	if (LocationReportApplication.isLogging(this)) {
			    		Log.d(TAG, "CONTACT_PICKER_RESULT: name => '" + name + ",', lookup_key => '" + key + "'");
			    	}
		    	}
	    	}
	    	finally {
	    		cursor.close();
	    	}
	    }
	}	

	private void addUser() {
	    startActivityForResult(new Intent(Intent.ACTION_PICK,
	    		ContactsContract.Contacts.CONTENT_URI), CONTACT_PICKER_RESULT);  
	}

	private void removeUser(int position) {
			Cursor cur = (Cursor) getListAdapter().getItem(position);
			mDbHelper.removeUser(cur.getString(cur.getColumnIndex(DatabaseHelper.U.KEY)));
			refreshList();
	}

	private void clearUsers() {
		mDbHelper.clearUsers();
		refreshList();
	}

	private void refreshList() {
		CursorAdapter adapter = (CursorAdapter) getListAdapter();
		Cursor cursor = adapter.getCursor(); 
		cursor.requery();
		
		adapter.notifyDataSetChanged();
		updateLayout();
	}

	private void updateLayout() {
		if (LocationReportApplication.isLite(this)) {
			TextView tv = (TextView) findViewById(R.id.upgrade_message);
			tv.setVisibility(mDbHelper.userRowsCount() == 0 ? View.GONE : View.VISIBLE);
		}
	}
}
