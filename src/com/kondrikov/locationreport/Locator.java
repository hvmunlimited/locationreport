package com.kondrikov.locationreport;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

public class Locator  implements LocationListener {
	private static final String TAG = Locator.class.getName();

	private static final long TWO_MINUTES = 120 * 1000;
	
	/** Minimum time interval for GPS notifications */
	private static final int UPDATE_DELTA = 10000;

	/** Minimun distance for GPS notifications */
	private static final float UPDATE_DISTANCE = 100;
	
	private Context mContext;
	
	private LocationManager mLocationManager;
	private Location mCurrentBestLocation;

	Locator(Context context) {
		mContext = context;
	}
	
	public void startLocate() {
		if (LocationReportApplication.isLogging(mContext)) {
			Log.d(TAG, "Start obtaining location");
		}
		
		mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);

		setLocation(mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
		if (getLocation() == null)
			setLocation(mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));

		if (getLocation() != null) {
			if (LocationReportApplication.isDebugging()) {
				if (LocationReportApplication.isLogging(mContext)) {
					Log.d(TAG, "getLastKnownLocation: " + getLocation().toString());
				}
			}
		}
		
		if (!isActualLocation(getLocation())) {
			setLocation(null);
		}

		mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
		mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, UPDATE_DELTA, UPDATE_DISTANCE, this);
	}
	
	public void stopLocate() {
		mLocationManager.removeUpdates(this);

		if (LocationReportApplication.isLogging(mContext)) {
			Log.d(TAG, "Stop obtaining location");
		}
	}

	public void onLocationChanged(Location location) {
		if (LocationReportApplication.isLogging(mContext)) {
			if (location != null) {
				Log.d(TAG, "onLocationChanged: provider=" + location.getProvider());
			} else {
				Log.d(TAG, "onLocationChanged");
			}
		}
		
		if (isBetterLocation(location, getLocation())) {
			setLocation(location);
		}
	}

	public void onProviderDisabled(String provider) {
	}

	public void onProviderEnabled(String provider) {
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
	}
	
	/** Determines whether one Location reading is better than the current Location fix
	  * @param location  The new Location that you want to evaluate
	  * @param currentBestLocation  The current Location fix, to which you want to compare the new one
	  */
	private boolean isBetterLocation(Location location, Location currentBestLocation) {
	    if (currentBestLocation == null) {
	        // A new location is always better than no location
	        return true;
	    }

	    // Check whether the new location fix is newer or older
	    long timeDelta = location.getTime() - currentBestLocation.getTime();
	    boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
	    boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
	    boolean isNewer = timeDelta > 0;

	    // If it's been more than two minutes since the current location, use the new location
	    // because the user has likely moved
	    if (isSignificantlyNewer) {
	        return true;
	    // If the new location is more than two minutes older, it must be worse
	    } else if (isSignificantlyOlder) {
	        return false;
	    }

	    // Check whether the new location fix is more or less accurate
	    int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
	    boolean isLessAccurate = accuracyDelta > 0;
	    boolean isMoreAccurate = accuracyDelta < 0;
	    boolean isSignificantlyLessAccurate = accuracyDelta > 200;

	    // Check if the old and new location are from the same provider
	    boolean isFromSameProvider = isSameProvider(location.getProvider(),
	            currentBestLocation.getProvider());

	    // Determine location quality using a combination of timeliness and accuracy
	    if (isMoreAccurate) {
	        return true;
	    } else if (isNewer && !isLessAccurate) {
	        return true;
	    } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
	        return true;
	    }
	    return false;
	}

	/** Checks whether two providers are the same */
	private boolean isSameProvider(String provider1, String provider2) {
	    if (provider1 == null) {
	      return provider2 == null;
	    }
	    return provider1.equals(provider2);
	}

	/** Check whether location is not too old */
	private boolean isActualLocation(Location location) {
		boolean actual = false;
		
		if (location != null) {
			if (LocationReportApplication.isLogging(mContext)) {
				long delta = System.currentTimeMillis() - location.getTime();
				Log.d(TAG, "Time delta: " + Long.toString(delta));
			}

			if (System.currentTimeMillis() - location.getTime() <= TWO_MINUTES) {
				actual = true;
			}
		}
		
		if (!actual) {
			if (LocationReportApplication.isLogging(mContext)) {
				Log.d(TAG, "Location is old");
			}
		}
		
		return actual;
	}

	public synchronized Location getLocation() {
		return mCurrentBestLocation;
	}

	private synchronized void setLocation(Location location) {
		mCurrentBestLocation = location;
	}
	
}
