package com.kondrikov.locationreport;

import java.util.HashMap;
import java.util.Map;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;

public class SmsReceiver extends BroadcastReceiver {
	
	private static final String TAG = SmsReceiver.class.getName();

	@Override
	public void onReceive(Context context, Intent intent) {
		if (LocationReportApplication.isDebugging()) {
			if (LocationReportApplication.isLogging(context)) {
				Log.d(TAG, "onReceive: " + intent);
			}
		}
		
		Map<String, Request> messages = retrieveMessages(intent);

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		if (!prefs.getBoolean("enabled", true)) {
			return;
		}
		
		if (!prefs.getBoolean("roaming", true)) {
			TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
			if (tm.isNetworkRoaming())
				return;
		}
		
		String keyPhrase = prefs.getString("key", "GPS!");
		
		int abortCount = messages.size();
		
		for (String sender : messages.keySet()) {
			Request req = messages.get(sender);
			if (req.getContents().equals(keyPhrase)) {
				
				if (LocationReportApplication.isLogging(context)) {
					Log.d(TAG, "Query received");
				}
				
				Intent service = new Intent(context, LocationService.class);
				service.setAction(LocationService.START_SERVICE);
				service.putExtra("timestamp", req.getTimestamp());
				service.putExtra("sender", req.getSender());

				context.startService(service);
				
				--abortCount;
			}
		}
		
		if (abortCount == 0)
			abortBroadcast();
	}

	private Map<String, Request> retrieveMessages(Intent intent) {
		Map<String, Request> messages = new HashMap<String, Request>();
		
        Bundle bundle = intent.getExtras();
        if (bundle != null && bundle.containsKey("pdus")) {
            Object[] pdus = (Object[]) bundle.get("pdus");

            if (pdus != null) {
                for (int i = 0; i < pdus.length; ++i) {
                    SmsMessage msg = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    Request req = messages.get(msg.getOriginatingAddress());
                    
                    if(req == null) {
                    	Request newReq = new Request("", msg.getOriginatingAddress(), msg.getMessageBody(), msg.getTimestampMillis());
                        messages.put(msg.getOriginatingAddress(), newReq); 
                        
                    } else {
                    	req.appendContents(msg.getMessageBody());
                        messages.put(msg.getOriginatingAddress(), req);
                    }
                }
            }
        }
        
        return messages;
	}

}
