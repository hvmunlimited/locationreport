package com.kondrikov.locationreport;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.provider.ContactsContract.PhoneLookup;
import android.telephony.SmsManager;
import android.util.Log;

public class LocationService extends Service {
	
	private final static String TAG = LocationService.class.getName();
	
	public final static String START_SERVICE = "START_SERVICE";
	public final static String STOP_SERVICE = "STOP_SERVICE";
	
	private final static int LOCATION_TIMEOUT = 120;
	private Locator mLocator;
	
	private PowerManager.WakeLock mWakeLock = null;
	
	private DatabaseHelper mDbHelper;

	private List<Request> mReqs = new ArrayList<Request>();
	
	/** Called when service is starting */
	@Override
	public void onCreate() {
		if (LocationReportApplication.isLogging(this)) {
			Log.d(TAG, "onCreate");
		}
		
		acquireWakeLock();
		
		mDbHelper = new DatabaseHelper(this);
		mLocator = new Locator(this);
		mLocator.startLocate();

		// Stop service later
		setTimeout();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startid) {
		if (LocationReportApplication.isLogging(this)) {
			Log.d(TAG, "onStartCommand: " + intent);
		}
		
		if (intent != null && intent.getAction().equals(LocationService.STOP_SERVICE)) {
			Request stopReq = new Request();
			stopReq.setStartId(startid);
			mReqs.add(stopReq);

			// Compose and send reply to all queries
			new SendAndStopTask().execute(mLocator.getLocation());

			return START_STICKY;
		}
		
		Request req = new Request();
		req.setStartId(startid);

		if (intent != null) {
			String senderPhone = intent.getStringExtra("sender");
			Uri contactUri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(senderPhone));
			Cursor cursor = getContentResolver().query(contactUri, new String[] {PhoneLookup.LOOKUP_KEY, PhoneLookup.DISPLAY_NAME, PhoneLookup.NUMBER}, null, null, null);
			
			try {
				if (cursor.moveToFirst()) {
					String displayName = cursor.getString(cursor.getColumnIndex(PhoneLookup.DISPLAY_NAME));
					String lookupKey = cursor.getString(cursor.getColumnIndex(PhoneLookup.LOOKUP_KEY));
					String number = cursor.getString(cursor.getColumnIndex(PhoneLookup.NUMBER));
					
					if (LocationReportApplication.isLogging(this)) {
						Log.d(TAG, "key => " + lookupKey + ", name => " + stripPrivateData(displayName) + ", number => " + stripPrivateData(number));
					}
					
					if (mDbHelper.hasUser(lookupKey)) {
						req = new Request(displayName, senderPhone, null, intent.getLongExtra("timestamp", 0));
						req.setStartId(startid);

						if (LocationReportApplication.isLogging(this)) {
							Log.d(TAG, "Contact found");
						}
					}
				}
				
				if (req.isEmpty()) {
					String displayName = senderPhone;
					if (cursor.moveToFirst()) {
						displayName = cursor.getString(cursor.getColumnIndex(PhoneLookup.DISPLAY_NAME));
					}
					
					if (LocationReportApplication.isLogging(this)) {
						Log.d(TAG, "User is not in whitelist: " + stripPrivateData(displayName));
					}

					mDbHelper.insertHistoryItem(displayName, senderPhone, intent.getLongExtra("timestamp", 0), DatabaseHelper.H.STATUS_REFUSED, "", 0, 0);
				}

			}
			finally {
				cursor.close();
			}
		}
	
		mReqs.add(req);
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		if (!req.isEmpty() && prefs.getBoolean("sendInfo", true)) {
			sendReply(req.getName(), req.getSender(), req.getTimestamp(), getResources().getText(R.string.msg_information).toString(), null, false);
		}
		
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		if (LocationReportApplication.isLogging(this)) {
			Log.d(TAG, "onDestroy");
		}
		
		mLocator.stopLocate();
		
		releaseWakeLock();
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	/** Acquire device wake lock */
	private void acquireWakeLock() {
		if (mWakeLock == null) {
			PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
			mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
		}

		mWakeLock.acquire();
		if (LocationReportApplication.isLogging(this)) {
			Log.d(TAG, "Acquired wake lock");
		}
	}

	/** Release device wake lock */
	private void releaseWakeLock() {
		if (mWakeLock != null) {
			mWakeLock.release();

			if (LocationReportApplication.isLogging(this)) {
				Log.d(TAG, "Released wake lock");
			}

		} else {
			if (LocationReportApplication.isLogging(this)) {
				Log.e("Wake lock equals to null!", TAG);
			}
		}
	}

	/** Stop service if there are no more queries */
	private void shutdown() {
		if (LocationReportApplication.isLogging(this)) {
			Log.d(TAG, "Shutdown");
		}

		for (Request req : mReqs) {
			stopSelf(req.getStartId());
		}
	}

	/** Compose reply message
	 *   @param location Obtained location. Must not be null
	 */
	private StringBuilder composeReply(Location location) {
		StringBuilder reply = new StringBuilder();

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

		// Send latitude/longitude if address or link are not present in reply
		boolean sendRawLocation = true;
		
		if (prefs.getBoolean("sendAddress", true)) {
			final int attemptsCount = 6;
			boolean failed = false;

			for (int attempt = 0; attempt < attemptsCount; ++attempt) {
				try {
					Geocoder geo = new Geocoder(this);
					failed = false;

					List<Address> addrs = geo.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
					if (addrs != null && addrs.size() > 0) {
						Address addr = addrs.get(0);
						for (int i = 0; i < addr.getMaxAddressLineIndex(); ++i) {
							reply.append(addr.getAddressLine(i));
							reply.append('\n');
						}

						sendRawLocation = false;
					}
				}

				catch (IOException e) {
					if (LocationReportApplication.isLogging(this)) {
						Log.e(TAG, "Geocoder error (attempt " + Integer.toString(attempt + 1) + " of " + Integer.toString(attemptsCount) + ")");
					}

					failed = true;
				}
			
				if (!failed) {
					break;
				}
			}
		}
		
		
		if (prefs.getBoolean("sendAccuracy", true)) {
			if (location.hasAccuracy()) {
				reply.append(getResources().getText(R.string.msg_accuracy));
				reply.append(location.getAccuracy());
				reply.append(getResources().getText(R.string.meter));
				reply.append('\n');
			}
		}
		
		if (prefs.getBoolean("sendLink", true)) {
			reply.append(String.format(Locale.US, "http://maps.google.com/?q=%f,%f&z=16",
					location.getLatitude(), location.getLongitude()));
			
			sendRawLocation = false;
		}

		// Send latitude/longitude if address or link are not present in reply
		if (sendRawLocation) {
			reply.append(getString(R.string.msg_latitude));
			reply.append(location.getLatitude());
			reply.append('\n');			
			reply.append(getString(R.string.msg_longitude));
			reply.append(location.getLongitude());
		}
		
		return reply;
	}
	
	/** Send message with information about location */
	private void sendReply(String name, String sender, long timestamp, String reply, Location location, boolean writeHistory) {
		if (LocationReportApplication.isLogging(this)) {
			Log.d(TAG, "Reply to: " + stripPrivateData(sender) + ", timestamp: " + Long.toString(timestamp));
		}
		
		if (writeHistory) {
			String status = DatabaseHelper.H.STATUS_OK;
			if (location == null) {
				status = DatabaseHelper.H.STATUS_NO_MAP;
			}
			
			double longitude = location != null ? location.getLongitude() : 0;
			double latitude  = location != null ? location.getLatitude()  : 0;
			mDbHelper.insertHistoryItem(name, sender, timestamp, status, reply, latitude, longitude);

			if (LocationReportApplication.isLogging(this)) {
				Log.d(TAG, "Inserted history item");
			}
		}
		
		if (!sender.equals("1234")) {
			SmsManager manager = SmsManager.getDefault();
			ArrayList<String> msgTexts = manager.divideMessage(reply);
			manager.sendMultipartTextMessage(sender, null, msgTexts, null, null);

			if (LocationReportApplication.isLogging(this)) {
				Log.d(TAG, "Message sent");
			}
		}
	}

	/** Replace a string holding private data with '***' */
	private String stripPrivateData(String s) {
		return LocationReportApplication.isDebugging() ? s : "***";
	}

	/** Set timeout to stop LocationService after it expires */
	private void setTimeout() {
		if (LocationReportApplication.isLogging(this)) {
			Log.d(TAG, "Set timeout");
		}
		
		PendingIntent intent = PendingIntent.getBroadcast(this, 0, new Intent(this, StopServiceAlarmReceiver.class), 0);

		AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
		alarmManager.set(AlarmManager.RTC, 
				System.currentTimeMillis() + (LOCATION_TIMEOUT * 1000), intent);
	}
	
	/** AsyncTask to send reply and stop service */
	private class SendAndStopTask extends AsyncTask<Location, Void, StringBuilder> {
		private Location mLocation;
		
		@Override
		protected void onPreExecute() {
			if (LocationReportApplication.isLogging(LocationService.this)) {
				Log.d(TAG, "Send reply and stop service");
			}
		}

		/** Compose reply */
		@Override
		protected StringBuilder doInBackground(Location... params) {
			mLocation = params[0];
			
			if (LocationReportApplication.isLogging(LocationService.this)) {
				if (mLocation == null) {
					Log.d(TAG, "Could not obtain location");
				}
			}
			
			return (mLocation == null) ? new StringBuilder().append(getString(R.string.location_error))
									   : composeReply(mLocation);
		}

		/** Send reply to all queries */
		@Override
		protected void onPostExecute(StringBuilder reply) {
			if (LocationReportApplication.isLogging(LocationService.this)) {
				Log.d(TAG, "Send reply to all queries");
			}
			
			for (Request req : mReqs) {
				if (!req.isEmpty()) {
					sendReply(req.getName(), req.getSender(), req.getTimestamp(), reply.toString(), mLocation, true);
				}
			}
			
			shutdown();
		}
		
	}
	
}
